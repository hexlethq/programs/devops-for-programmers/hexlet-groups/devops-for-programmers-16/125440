variable "do_token" {
  description = "Digital ocean access token"
  type        = string
  sensitive   = true
}