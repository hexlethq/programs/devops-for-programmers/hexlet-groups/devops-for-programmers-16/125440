terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "devops" {
  name = "devops"
}

resource "digitalocean_droplet" "web1" {
  image    = "docker-20-04"
  name     = "web-terraform-homework-01"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.devops.id]
}

resource "digitalocean_droplet" "web2" {
  image    = "docker-20-04"
  name     = "web-terraform-homework-02"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.devops.id]
}

resource "digitalocean_certificate" "cert" {
  name    = "cert"
  type    = "lets_encrypt"
  domains = ["somedomain.club"]
}

resource "digitalocean_loadbalancer" "public" {
  name   = "loadbalancer-1"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = 5000
    target_protocol = "http"

    certificate_name = digitalocean_certificate.cert.name
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  droplet_ids = [digitalocean_droplet.web1.id, digitalocean_droplet.web2.id]
}

resource "digitalocean_domain" "default" {
  name       = "somedomain.club"
  ip_address = digitalocean_loadbalancer.public.ip
}