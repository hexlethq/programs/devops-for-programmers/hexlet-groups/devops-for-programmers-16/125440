variable "do_token" {
  description = "Digital ocean access token"
  type        = string
  sensitive   = true
}

variable "region" {
  description = "Digital ocean region name"
  type = string
  default = "ams3"
}

variable "app_port" {
  description = "App port"
  type = string
  default = 8080
}
