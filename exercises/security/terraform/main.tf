data "digitalocean_ssh_key" "devops" {
  name = "devops"
}

resource "digitalocean_vpc" "vpc" {
  name   = "example-project-network"
  region = var.region
}

resource "digitalocean_droplet" "bastion" {
  image    = "ubuntu-18-04-x64"
  name     = "bastion"
  size     = "s-1vcpu-1gb"
  region   = var.region
  private_networking = true
  vpc_uuid = digitalocean_vpc.vpc.id
  ssh_keys = [data.digitalocean_ssh_key.devops.id]
}

resource "digitalocean_droplet" "web" {
  count    = 2
  image    = "docker-20-04"
  name     = format("app-%02d", count.index + 1)
  region   = var.region
  size     = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys = [data.digitalocean_ssh_key.devops.id]
  vpc_uuid = digitalocean_vpc.vpc.id
}

resource "digitalocean_loadbalancer" "public" {
  name   = "loadbalancer-1"
  region = var.region

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = var.app_port
    target_protocol = "http"
  }

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = var.app_port
    target_protocol = "http"

    certificate_name = digitalocean_certificate.cert.name
  }

  healthcheck {
    port     = var.app_port
    protocol = "http"
    path     = "/"
  }

  droplet_ids = digitalocean_droplet.web.*.id
  vpc_uuid    = digitalocean_vpc.vpc.id
}

resource "digitalocean_domain" "default" {
  name = "devandops.club"
}

resource "digitalocean_certificate" "cert" {
  name    = "project-cert"
  type    = "lets_encrypt"
  domains = [digitalocean_domain.default.name]
}

resource "digitalocean_firewall" "web" {
  name        = "web-firewall"
  droplet_ids = digitalocean_droplet.web.*.id

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_droplet_ids = [digitalocean_droplet.bastion.id]
  }

  outbound_rule {
    protocol                = "tcp"
    port_range              = "53"
    destination_addresses   = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol                = "tcp"
    port_range              = var.app_port
    source_addresses   = ["10.110.16.0/20"]
  }

  outbound_rule {
    protocol                = "tcp"
    port_range              = var.app_port
    destination_addresses   = ["10.110.16.0/20"]
  }

  inbound_rule {
    protocol              = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol                = "icmp"
    destination_addresses   = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "80"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "443"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_firewall" "bastion" {
  name        = "bastion-firewall"
  droplet_ids = [digitalocean_droplet.bastion.id]


  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol                = "tcp"
    port_range              = "22"
    destination_droplet_ids = digitalocean_droplet.web.*.id
  }

  inbound_rule {
    protocol           = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol                = "icmp"
    destination_droplet_ids = digitalocean_droplet.web.*.id
  }
}

output "ip_addresses" {
  value = digitalocean_droplet.web.*.ipv4_address
}
